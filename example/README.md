# Usage example

Author's usecase: installing VoidLinux with script.
It uses almost all features that already implemented.

These are maqet script, configuration scripts and config used in [demo](https://youtu.be/8WFoWOeJKQo)
Some changes were made for code readability.
This example is a temporary substitution for documentation.

## Script (vm.py)

All decorations (fancy console outputs) are removed.
Running is blocked by exit() as a safety measure

## Config (vm_config.yml)

Some lines are commented for clarification.
virtfs argument is used to mount directory from host FS into VM

Arguments section parser can parse in deep, arguments could be added like this:

```yaml
arguments:
- arg: foo
  bar: baz
  spam:
    eggs: 1
    not_eggs: 2
    also_not_eggs:
      very_deep_argument: still_works
```

And parsed argument looks like this:

`-arg foo,bar=baz,spam.eggs=1,spam.not_eggs=2,spam.also_not_eggs.very_deep_argument=still_works`

Feature: you can create qcow2 images that are backed by another image
(read [QEMU wiki](https://wiki.qemu.org/Documentation/CreateSnapshot#Modifying_the_backing_file))

To use this, add `backing` key to drive

```yaml
storage:
hda:
  ...
  backing: <path_to_qcow2_image>
```

## Scripts (everything with .sh)

These scripts should be placed in a directory which is mounted with virtfs.
Also, ROOTFS.tar.gz should be placed there.
It can be downloaded from voidlinux.org

## Running script

Script called like this:

```bash
python vm.py -c vm_config.yml -s /tmp/storage/maqet install config 
```

### CLI arguments method

Objects of `Maqet` have `cli()` method. When called, command line arguments are parsed.

Arguments:

- stage: names of stages to run.
The order of execution is determined by order of arguments

Options:

- -c CONFIG_FILE, --config-file CONFIG_FILE: Pass yaml file as a config
- -a=ARGUMENT, --argument=ARGUMENT: add plain qemu argument,
use with equal and quotes: `-a='-display=gtk,show-menubar=off'`
- -s STORAGE_PATH, --storage-path STORAGE_PATH: Set default path to save drive files
In demo it was set to `/tmp/maqet/storage` to keep drive files on RAM (tmpfs mount)
and not clutter physical drive
- -v, --verbose: increase verbose level, adding `-vvvv` will show all internal
maqet debug logs
- --command: Output qemu command with all arguments and exit. Used for debugging

### Hard-coded method

Object of `Maqet` called directly, names of stages passed as positional arguments.
The order of execution is determined by order of arguments

```python
m = Maqet(**CONFIG)
...
m('install', 'config', 'run')
```
