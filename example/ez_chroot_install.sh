#! /bin/bash
xbps-install -Suy xbps
xbps-install -uy

# Install all packages at once
# In case of errors - try harder
until xbps-install -Sy base-system grub NetworkManager wayland mesa-dri elogind polkit-elogind dbus-elogind seatd sway foot neofetch emptty xorg-minimal
do
  xbps-install -uy
done
# This one can't be installed in loop, it always returns non-zero rc 
xbps-install -y xorg-fonts

# Setting hostname. adding user and doing all that stuff
echo void_vm.local > /etc/hostname
xbps-reconfigure -f glibc-locales
useradd anon
echo -e "voidlinux\nvoidlinux" | passwd anon
cat /proc/mounts | grep /dev/sda >> /etc/fstab
grub-install /dev/sda --no-nvram
echo '1.1.1.1' > /etc/resolv.conf

# Enable services
ln -s /etc/sv/sshd /etc/runit/runsvdir/default/
ln -s /etc/sv/NetworkManager /etc/runit/runsvdir/default/
ln -s /etc/sv/dbus /etc/runit/runsvdir/default/
xbps-reconfigure -fa
