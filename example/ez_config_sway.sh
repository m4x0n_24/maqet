#! /bin/bash

# Enable services
ln -s /etc/sv/dbus /etc/runit/runsvdir/default/
# ln -s /etc/sv/elogind /etc/runit/runsvdir/default/
ln -s /etc/sv/polkitd /etc/runit/runsvdir/default/
ln -s /etc/sv/seatd /etc/runit/runsvdir/default/

# Set mod key to alt for sway
sed -i 's/set \$mod Mod4/set \$mod Mod1/' /etc/sway/config

# Adding emptty settings to start sway
touch /home/anon/.emptty
cat <<EOF > /home/anon/.emptty
Name=Sway 
Exec=/usr/bin/sway 
Environment=wayland
EOF
chown anon:anon /home/anon/.emptty

# Enable emptty service and shutdown. Service enables immidiately and logs user off
# so shutdown should be made within same command
ln -s /etc/sv/emptty /etc/runit/runsvdir/default/ && shutdown -hf now
