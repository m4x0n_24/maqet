#! /bin/bash
set -e # Exit on first error

# Making 2 partitions with sfdisk and formatting them
BOOT_SIZE="512M"
echo -e ",$BOOT_SIZE,L,*\n,+," | sfdisk /dev/sda
mkfs.vfat /dev/sda1
mkfs.ext4 /dev/sda2

# Mounts and typical ROOTFS void linux installation
mount /dev/sda2 /mnt/
mkdir -p /mnt/boot/efi/
tar xvf ROOTFS.tar.gz -C /mnt
mount --mkdir /dev/sda1 /mnt/boot/efi

mkdir -p "/mnt/var/db/xbps/keys"
cp /var/db/xbps/keys/* "/mnt/var/db/xbps/keys"

# Putting chroot script into root and executing it within chroot
cp ez_chroot_install.sh /mnt/root
chmod u+x /mnt/root/ez_chroot_install.sh
xchroot /mnt /root/ez_chroot_install.sh

# Finish. Shutdown
umount -R /mnt
shutdown -hf now
