import time

import paramiko.util
from benedict import benedict
from paramiko import AutoAddPolicy, SSHClient, SSHException

from maqet import Maqet

paramiko.util.log_to_file('/dev/null')

SSH_USER = "anon"
SSH_PASS = "voidlinux"
SSH_PORT = 2222


class Ssh:
    def __init__(self, retries=100):
        self._client = SSHClient()
        self._client.set_missing_host_key_policy(AutoAddPolicy())

        while retries > 0:
            try:
                self._client.connect(
                    hostname='localhost',
                    port=SSH_PORT,
                    username=SSH_USER,
                    password=SSH_PASS,
                    look_for_keys=False,
                    allow_agent=False,
                )
                break
            except SSHException:
                retries -= 1
                if retries > 0:
                    print(f"Not connected. {retries} retries left")
                    time.sleep(1)
                else:
                    exit("SSH not connected. Something went wrong")

    def __call__(self, command):
        stdin, stdout, stderr = self._client.exec_command(
            command
        )
        stdin.close()
        return benedict({
            'command': command,
            'stdout': stdout.read().decode('ascii').strip("\n"),
            'stderr': stderr.read().decode('ascii').strip("\n")
        })


m = Maqet()


def launch_and_login(vm: m.Machine):
    vm.launch()
    time.sleep(1)
    vm.send_input('\n')

    ssh = Ssh()  # wait for ssh connection

    print('Connected, logging in')
    vm.send_input("root\n")  # Login
    time.sleep(0.1)
    vm.send_input(SSH_PASS + "\n")  # Login
    time.sleep(1)
    return ssh


@m.stage
def install(vm: m.Machine):
    vm.storage['hda'].snapshot('init')
    vm.add_args('-cdrom ../live.iso')

    launch_and_login(vm)
    vm.send_input('mount -t 9p -o trans=virtio media /media\n')
    vm.send_input("cd /media\rchmod u+x ./ez_install_script.sh\r"
                  "./ez_install_script.sh\r")

    while vm.is_running():
        time.sleep(10)
    print("Stage finished")
    vm.storage['hda'].snapshot('installed', overwrite=True)


@m.stage
def config(vm: m.Machine):
    if 'installed' not in vm.storage['hda'].snapshots:
        exit("No snapshot 'installed', is install stage finished?")

    vm.storage['hda'].snapshot('installed')

    launch_and_login(vm)

    vm.send_input('mount -t 9p -o trans=virtio media /media\n')
    vm.send_input("chmod u+x /media/ez_config_sway.sh && "
                  "/media/ez_config_sway.sh\n")

    while vm.is_running():
        time.sleep(5)
    print("Stage finished")
    vm.storage['hda'].snapshot('configured', overwrite=True)
    time.sleep(1)


@m.stage
def run(vm: m.Machine):
    if 'configured' not in vm.storage['hda'].snapshots:
        exit("No snapshot 'configured', is install stage finished?")

    vm.launch()
    time.sleep(10)

    input("Press ENTER to finish")
    vm.shutdown()


if __name__ == '__main__':
    exit("This script is for for informational purposes only."
         "Rewrite config and functions according to your environment")
    m.cli()
